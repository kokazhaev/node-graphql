const { buildSchema } = require('graphql');

module.exports = buildSchema(`
  type Query {
    getAllUsers: [User]!
  }
  type User {
    id: ID!
    name: String!
    age: Int!
    admin: Boolean!
  }
  type Mutation {
    addUser(name: String! age: Int! admin: Boolean!): String
    removeUser(id: Int!): String
    updateUser(id: Int! name: String! age: Int!): String
  }
`)