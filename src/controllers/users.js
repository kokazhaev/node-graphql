const UserModel = require('../models/user');

// get all users
const getAllUsers = async () => {
  try {
    const users = await UserModel.findAll();
    console.log('users', users)
    return users
  } catch (error) {
    console.log('err', error)
    return error
  }
}
// add new user
const addUser = async ({name, age, admin=false}) => {
  try {
    const newUser = await UserModel.create({ name: name, age: age, admin: admin })
    return newUser.get('id');
  } catch (error) {
    console.log(error)
    return error
  }
}
// delete user
const removeUser = async ({id}) => {
  try {
    const removedUser = await UserModel.destroy({ where: { id: id } });
    console.log('delete', removedUser)  
    return 'Deleted';
  } catch(error) {
    console.log(error);
    return error
  }
}
// change user
const updateUser = async ({id, name, age}) => {
  try {
    const updatedUser = await UserModel.update({ name: name, age: age }, { where: { id: id }})
    console.log('willUpdateUser', updatedUser);
    return `updated`
  } catch(error) {
    console.log('error', error)
    return error
  }
}

module.exports = {
  addUser,
  getAllUsers,
  removeUser,
  updateUser
}