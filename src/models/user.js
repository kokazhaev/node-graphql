const { DataTypes } = require('sequelize');
const sequelize = require('../db');

// создает таблицу в базе (если нету)
sequelize.sync().then(result=>{
  console.log('res', result);
})
.catch(err=> console.log('err', err));

const User = sequelize.define('user', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  age: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  admin: {
    type: DataTypes.BOOLEAN,
    allowNull: false
  }
});

module.exports = User;