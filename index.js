const express = require('express');
const graphqlHTTP = require('express-graphql');

const app = express();

const schema = require('./src/schema');
const { getAllUsers, addUser, removeUser, updateUser } = require('./src/controllers/users');

const PORT = process.env.PORT || 5000;
const root = {
  addUser: addUser,
  getAllUsers: getAllUsers,
  removeUser: removeUser,
  updateUser: updateUser
}

app.use(graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

app.listen(PORT, () => {
  console.log(`Server runnin on port ${PORT}`)
});